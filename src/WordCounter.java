/**
 * Dhimitris Natsis and Naishadh Amin
 * Software Analysis and Design
 * Word Counter
 */
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


import javax.swing.JFileChooser;

public class WordCounter
{
public static void main(String[] args) throws IOException
{
/**
 * Import the .txt file using JFileChooser
 */
	JFileChooser chooser = new JFileChooser();
	
    int returnVal = chooser.showOpenDialog(null);
    if(returnVal == JFileChooser.APPROVE_OPTION) {
    	
    }
    File file = chooser.getSelectedFile();

//analyze the file
WordFrequencyAnalyzer analyzer =
new WordFrequencyAnalyzer();
analyzer.analyzeText(file);

//get and display the results
WordFrequencyCollection collection = analyzer.getResults();
//user file writer to write the outline for the csv file
FileWriter writer = new FileWriter("WordFrequency.csv");
	writer.append("Word");
	writer.append(',');
	writer.append("Frequency");
	writer.append('\n');
	writer.append('\n');
for(String word : collection) {
	

try
{
	
	//write the word and frequency in the csv file in ascending order
  writer.append(word);
  writer.append(',');
	writer.append(String.valueOf(collection.getFrequency(word)));
	writer.append('\n');
   
}
catch(IOException e)
{
     e.printStackTrace();
} 

}
writer.flush();
writer.close();
}
}