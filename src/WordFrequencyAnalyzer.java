/**
 * Dhimitris Natsis and Naishadh Amin
 * Software Analysis and Design
 * Word Frequency Analyzer
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class WordFrequencyAnalyzer {

	private WordFrequencyCollection data;
	
	public WordFrequencyAnalyzer(){
		data = new WordFrequencyCollection();
	}
/** finds all the words in the given file and computes their frequencies
 * precondition: file corresponds to a readable file 
@param file the File whose words are to be analyzed */
	
	public void analyzeText(File file){
		Scanner scanner = null;
		try {
			scanner = new Scanner(file);
		}
		catch (FileNotFoundException e){
			e.printStackTrace();
		}
		
		while (scanner.hasNext()){
			String word = scanner.next();
			data.add(word);
		}
	}
	// returns data
	public WordFrequencyCollection getResults() {
		return data;
	}
}

