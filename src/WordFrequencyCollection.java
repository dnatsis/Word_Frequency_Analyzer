/**
 * Dhimitris Natsis and Naishadh Amin
 * Software Analysis and Design
 * Word Frequency Collection
 */
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;


public class WordFrequencyCollection implements Iterable<String> {
	
	private HashMap<String,Integer>data;

public WordFrequencyCollection() {
	data = new HashMap<String,Integer>();
	}
/** adds each individual word to the Hashmap
 * precondition: file corresponds to a string word 
@param data the hashmap in which the word is added */
public void add(String word) {
	if (data.containsKey(word)) data.put(word, data.get(word)+1);
	else data.put(word, 1);
}
/** returns a statement with amount of total words
 * precondition: none 
@param data.size to number of words printed */
public String toString() {
	return "WordFrequencyCollection with " + data.size() + "words";
}
/** returns the frequency of a word
 * precondition: file corresponds to a string word 
@param data the hashmap in which the word is stored in */
public int getFrequency(String word){
	return data.get(word);
}

/** iterates the hashmap containing all the words from our txt file
 * precondition: file corresponds to a string word 
@param data the hashmap in which the words are going to be sorted accordingly */
public Iterator iterator() { return new Iterator() {
	String words[] = data.keySet().toArray(new String[data.size()]);
	int index = 0;
	{
		//sort based on the new comparator that is being initialized within the call to sort
		Arrays.sort(words, new Comparator<String>(){
			public int compare(String o1, String o2) {
				int order = data.get(o1) - data.get(o2);
				if(order !=0) return order;
				else return o1.compareTo(o2);
			}
		});
	}
	//iterator hasnext() method
public boolean hasNext() {
	return index<words.length;
}
//iterator next() method
public String next() {
	return words[index++];
}
//iterator remove() method
public void remove() {
	throw new UnsupportedOperationException();
}
};
}}
